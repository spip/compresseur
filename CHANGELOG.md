# Changelog

## 2.3.0 - 2025-11-25

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- !4877 spip/spip!5951 Utiliser la fonction `svg_nettoyer()` introduite dans SPIP.

### Removed

- !4878: Suppression de la constante _DIR_RESTREINT_ABS
